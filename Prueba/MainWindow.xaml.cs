﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AsfiTracing.Data.Model;

namespace Prueba
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			var initialRowCount = 0;
			var executioncycle = 1;
			using (AsfiTracing.Data.Context.DB db0 = new AsfiTracing.Data.Context.DB())
			//using (var db0 = new AsfiTracing.Data.Context.DB())
			{
				var cubes = db0.Cube.ToList();
				var entityTypes = db0.EntityType.ToList();
				var frequencies = db0.Frequency.ToList();
				var tracings = db0.Tracing.ToList();
				foreach (var tracing in tracings)
				{
					//Console.WriteLine(cube.code + " " + cube.job + " " + cube.name);
					//Console.WriteLine(testTable.Name + " " + testTable.Id);
					//Console.WriteLine(entityType.entitytype + " " + entityType.name);
					//Console.WriteLine(frequency.code+ " " + frequency.datafrequency);
					Console.WriteLine(tracing.codespim + " " + tracing.entitytype);
				}
				// test insert entityType
				/*var tet = new AsfiTracing.Data.Model.EntityType { entitytype = "05", name = "Fondo" };
				db0.EntityType.Add(tet);
				db0.SaveChanges();*/
				// test insert cube
				/*var tc = new AsfiTracing.Data.Model.Cube { code = "S_BOEIF_99_00005", job = "Crear cubo Estr-Dep-Depto-TipoPers-Saldos-Cuentas", name = "Estratificación de Depósitos por Departamento y Tipo de Persona (Saldos y Cuentas)", module = "Depósitos/Captaciones" };
				db0.Cube.Add(tc);
				db0.SaveChanges();
				*/
				//test();
			}
			var newTracing = new Tracing()
			{
				idtracing = "600",
				codespim = "S_prueba_99_00001",
				entitytype = "100",
				dateto = new DateTime(),
				downloaddate = new DateTime(),
				creationdate = new DateTime(),
				client = "Banco de la Comunidad"
			};
			using (AsfiTracing.Data.Context.DB db1 = new AsfiTracing.Data.Context.DB())
			{
				// Agregar los articulos al DbSet
				db1.Tracing.Add(newTracing);
				//bd.Articulos.Add(articulo2);
				// Enviar cambios a la Base de Datos
				db1.SaveChanges();
			}


		}
		public static void test()
		{
			// The files used in this example are created in the topic
			// How to: Write to a Text File. You can change the path and
			// file name to substitute text files of your own.

			// Example #1
			// Read the file as one string.
			string text = System.IO.File.ReadAllText(@"c:\Publico\Logs Santos\20180319_La promotora\seguimiento.txt");

			// Display the file contents to the console. Variable text is a string.
			System.Console.WriteLine("Contents of WriteText.txt = {0}", text);

			// Example #2
			// Read each line of the file into a string array. Each element
			// of the array is one line of the file.
			string[] lines = System.IO.File.ReadAllLines(@"c:\Publico\Logs Santos\20180317_La promotora\seguimiento.txt.txt");

			// Display the file contents by using a foreach loop.
			System.Console.WriteLine("Contents of WriteLines2.txt = ");
			foreach (string line in lines)
			{
				// Use a tab to indent each line of the file.
				Console.WriteLine("\t" + line);
			}

			// Keep the console window open in debug mode.
			Console.WriteLine("Press any key to exit.");
			System.Console.ReadKey();
		}
	}
}
