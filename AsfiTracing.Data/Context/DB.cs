﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using AsfiTracing.Data.Model;


namespace AsfiTracing.Data.Context
{
	public class DB : DbContext
	{
		public DB()
			: base("PostgreeDbContext")
		{
			
			Database.SetInitializer<DB>(null);
#if DEBUG
			Database.Log = Console.Write;
#endif
		}

		public DbSet<TestTableRow> TestTable { get; set; }
		public DbSet<Cube> Cube { get; set; }
		public DbSet<EntityType> EntityType { get; set; }
		public DbSet<Frequency> Frequency { get; set; }
		public DbSet<Tracing> Tracing { get; set; }
	}
}
