﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Diagnostics;
using System.IO;
using System.Linq;
using CsvHelper;
using CsvHelper.Configuration;

//using log4net;
//using log4net.Config;




namespace AsfiTracing.Data.Model
{
	[Table("entitytype", Schema = "public")]
	public class EntityType
	{
		[Key]
		[Column("entitytype")]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public string entitytype { get; set; }
		public string name { get; set; }
	}
}
