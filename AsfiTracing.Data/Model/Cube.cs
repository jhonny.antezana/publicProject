﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Diagnostics;
using System.IO;
using System.Linq;
using CsvHelper;
using CsvHelper.Configuration;

namespace AsfiTracing.Data.Model
{
	[Table("cube", Schema = "public")]
	public class Cube
	{
		[Key]
		[Column("code")]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public string code { get; set; }

		public string job { get; set; }

		public string name { get; set; }

		public string module { get; set; }
	}
}
