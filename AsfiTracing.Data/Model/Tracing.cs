﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Diagnostics;
using System.IO;
using System.Linq;
using CsvHelper;
using CsvHelper.Configuration;

namespace AsfiTracing.Data.Model
{
	[Table("tracing", Schema = "public")]
	public class Tracing
	{
		[Key]
		[Column("id_tracing")]
		//[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public string idtracing { get; set; }
		public string codespim { get; set; }

		public string entitytype { get; set; }

		public DateTime dateto { get; set; }
		public DateTime downloaddate { get; set; }
		public DateTime creationdate { get; set; }
		public string client { get; set; }
	}
}
